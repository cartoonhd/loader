const detectRobot = (userAgent) => {
    const robots = new RegExp([
      /bot/,/spider/,/crawl/,                            // GENERAL TERMS
      /APIs-Google/,/AdsBot/,/Googlebot/,                // GOOGLE ROBOTS
      /mediapartners/,/Google Favicon/,
      /FeedFetcher/,/Google-Read-Aloud/,
      /DuplexWeb-Google/,/googleweblight/,
      /bing/,/yandex/,/baidu/,/duckduck/,/yahoo/,        // OTHER ENGINES
      /ecosia/,/ia_archiver/,/msn/,/teoma/,
      /facebook/,/instagram/,/pinterest/,/reddit/,       // SOCIAL MEDIA
      /slack/,/twitter/,/whatsapp/,/youtube/,
      /semrush/,/trident/,/slurp/,/google/                                         // OTHER
    ].map((r) => r.source).join("|"),"i");               // BUILD REGEXP + "i" FLAG
  
    return robots.test(userAgent);
  };